﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using DungeonForms.entities;

delegate void CrossThreading();     //delegates at same level as namespace. This delegates handles communication between threads
namespace DungeonForms
{
    public partial class Form1 : Form, Renderer
    {
        Game game;
        Helper helper = Helper.getInstance();
        Canvas canvas;
        AnimationLoop aniLoop;
        ResourceCollection r;
        Font messageFont;
        Brush messageBrush;
        public const int MARGIN = 16;
        public const int NEWLINE = 32;
        public const int TILE_SIZE = 32;
        public MessageContainer container = MessageContainer.getContainer();
        public int WindowMode = 0;      //0 is exploration mode, 1 is map-mode. Map mode will only work for forms
        private ImageAttributes attribsCurrentRoomOnMap;
        private ImageAttributes defaultAttribs;
        CrossThreading mousePos;
        CrossThreading redraw;
        private RectangleF MouseHitBoxRect = new RectangleF(0,0,1,1);
        Label lookAtLabel;      //status label shown below
        Label extraInfoLabel;   //label showing stuff like equipment etc to the right (lists stuff)

        //what is currently shown in the right column of the screen
        const string EXTRA_NONE = "None";
        const string EXTRA_MAP = "Map";     //map mode wont allow inventory management etc
        const string EXTRA_GEAR = "Gear";
        const string EXTRA_GEAR_STATS = "GearStats";
        string currentExtraMode = EXTRA_NONE;      //can be "None", "Gear", determines what is shown in column to the right

        //The type of item shown in the screen of the currently selected item. Check constants in Game-class
        string currentGearStats;


        //for grabbing the right bitmap for the map mode
        public const string MAP_N = "map_n";
        public const string MAP_S = "map_s";
        public const string MAP_W = "map_w";
        public const string MAP_E = "map_e";
        public const string MAP_EW = "map_ew";
        public const string MAP_NS = "map_ns";
        public const string MAP_NW = "map_nw";
        public const string MAP_SW = "map_sw";
        public const string MAP_NE = "map_ne";
        public const string MAP_SE = "map_se";
        

        public Form1()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;        //maximizes window at startup

            messageBrush = new SolidBrush(Color.Gray);
            messageFont = new Font("Arial",12f);
            defaultAttribs = new ImageAttributes();
            attribsCurrentRoomOnMap = new System.Drawing.Imaging.ImageAttributes();
            float[][] colorMatrixElements = { 
                               new float[] {1,  0,  0,  0, 0},        // red scaling factor of 1
                               new float[] {0,  0,  0,  0, 0},        // green scaling factor of 0
                               new float[] {0,  0,  0,  0, 0},        // blue scaling factor of 0
                               new float[] {0,  0,  0,  1, 0},        // alpha scaling factor of 1
                               new float[] {0,  0,  0,  0, 0}};      

            ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);

            attribsCurrentRoomOnMap.SetColorMatrix(
               colorMatrix,
               ColorMatrixFlag.Default,
               ColorAdjustType.Bitmap);



            canvas = new Canvas(this);
            

            canvas.Dock = DockStyle.Fill;

            lookAtLabel = new Label();
            lookAtLabel.Text = "test";
            lookAtLabel.Font = messageFont;
            lookAtLabel.ForeColor = Color.Gray;

            extraInfoLabel = new Label();
            extraInfoLabel.Font = messageFont;
            extraInfoLabel.ForeColor = Color.White;
            extraInfoLabel.Dock = DockStyle.Fill;
            extraInfoLabel.Margin = new Padding(0,MARGIN,0,0);

            tableLayoutPanel1.BackColor = Color.Black;
            tableLayoutPanel1.Controls.Add(canvas, 0, 0);
            tableLayoutPanel1.Controls.Add(lookAtLabel, 0, 1);
            tableLayoutPanel1.Controls.Add(extraInfoLabel, 1, 0);

            displayInExtraColumn(EXTRA_NONE,null);   //displaying default view in right column
           
            

            r = ResourceCollection.getResourceCollection();
            r.AddToCollection(ResourceCollection.SPLATTER,Properties.Resources.splatter);

            game = new Game();
            


            //delegates
            mousePos = delegate()
            {
                getMousePos();
            };

            redraw = delegate()
            {
                invokeRefresh();
            };

            aniLoop = new AnimationLoop(this);
            canvas.MouseClick += canvas_MouseClick;

        }

        void canvas_MouseClick(object sender, MouseEventArgs e)
        {
           
            if (e.Button == MouseButtons.Left && e.Clicks == 1)     //RANGED ATTACK
            {

                string attackMethod = game.player.getAttackType();
                Game.Debug_Mess("current attackmethod " + attackMethod);

                if (attackMethod == Entity.ATTACK_RANGED_ONEHAND ||      //REGULAR RANGED
                    attackMethod == Entity.ATTACK_RANGED_TWOHAND)
                {
                    Entity ent = getMousePos();
                    if (ent != null)
                    {
                        if (!(ent is Player))
                        {
                            bool didItWork = game.player.rangedInteraction(ent, game.logics,attackMethod);

                            if (didItWork)
                            {
                                game.logics.moveActors();
                                canvas.Refresh();
                            }
                            else
                            {

                                canvas.Refresh();       //i want refresh here to display the "couldnt attack" message
                                container.clearMessages();
                            }
                        }
                    }
                }

                else if (attackMethod == Entity.ATTACK_THROWABLE_ONEHAND ||     //throwable ranged
                    attackMethod == Entity.ATTACK_THROWABLE_TWOHAND)
                {


                    Entity ent = getTilePosition();

                    if (ent != null)
                    {

                        if (ent.xPos == game.player.xPos && ent.yPos == game.player.yPos)       //player targetted himself
                        {
                            List<Entity> creatureList = game.player.aoeAttack(game.logics, 1, new int[] { ent.xPos, ent.yPos });

                            foreach (Entity creature in creatureList)
                            {
                                game.player.combat(creature, game.logics,attackMethod);
                                game.logics.moveActors();
                                canvas.Refresh();
                            }

                            return;
                        }

                        if (game.player.checkLoSAoE(ent.xPos, ent.yPos, game.player.xPos, game.player.yPos, game.logics))    //checking LoS
                        {
                            List<Entity> creatureList = game.player.aoeAttack(game.logics, 1, new int[] { ent.xPos, ent.yPos });

                            foreach (Entity creature in creatureList)
                            {
                                game.player.combat(creature, game.logics,attackMethod);
                                game.logics.moveActors();
                                canvas.Refresh();
                            }
                        }
                        else
                        {
                            container.clearMessages();
                            container.addMessage("Something in the way");
                            canvas.Refresh();
                        }
                    }

                }

                //TODO ranged spell


            }


            //Right click
            else if (e.Button == MouseButtons.Right && e.Clicks == 1)
            {
                //Nothing here
                
            }
        }




        //getting number pressed for example equipment => should show gear stats
        public bool getNumberPressed(int number)
        {
            List<Entity> listToCheck = null;

            if (currentExtraMode == EXTRA_GEAR)
            {
                listToCheck = game.player.inventory;
                if (number >= listToCheck.Count)
                {
                    return false;
                }
                else
                {
                    game.player.currentlySelectedItem = listToCheck[number] as Item;
                    displayInExtraColumn(EXTRA_GEAR_STATS, listToCheck[number] as Item);
                }
            }


            return false;
        }


        /// <summary>
        /// Displays all equipment in the right column
        /// </summary>
        public void displayInExtraColumn(string mode, Entity selectedItem)
        {
            switch (mode)
            {
                case EXTRA_NONE: {      //closing menu, showing default
                    currentExtraMode = EXTRA_NONE;
                    extraInfoLabel.Text = "'m' Map\n" +
                        "'i' Inventory";
                    break; 
                }

                case EXTRA_MAP:
                    {      //closing menu, showing default
                        currentExtraMode = EXTRA_MAP;
                        extraInfoLabel.Text = "MAP\n\n" +
                            "'c' Close view\n";
                        break;
                    }

                case EXTRA_GEAR_STATS:      //specific item menu
                    {
                        currentExtraMode = EXTRA_GEAR_STATS;
                        if (selectedItem != null) {

                            if (game.logics.checkIfItemCanBeEquipped(selectedItem as Item, false, out currentGearStats))  //if item equippable?
                            {
                                if (game.logics.checkIfItemIsEquipped(selectedItem as Item, false, out currentGearStats)) //is it equipped already?
                                {

                                }

                            }

                            if (currentGearStats == Game.GEAR_STATS_EQUIPPED)   //currently equipped
                            {
                                Item gear = selectedItem as Item;
                                extraInfoLabel.Text = gear.name + " *Equipped" + "\n\n" +
                                    "'c' Close view\n" +
                                    "'d' Drop item\n" +
                                    "'u' Unequip item";
                            }
                            else if (currentGearStats == Game.GEAR_STATS_EQUIPPABLE){   //not equipped
                                Item gear = selectedItem as Item;
                                extraInfoLabel.Text = gear.name + "\n\n" +
                                    "'c' Close view\n" +
                                    "'d' Drop item\n" +
                                    "'e' Equip item";
                            }
                            else if(currentGearStats == Game.GEAR_STATS_POTION){        //usable item
                                Item gear = selectedItem as Item;
                                extraInfoLabel.Text = gear.name + "\n\n" +
                                    "'c' Close view\n" +
                                    "'u' Use item\n" + 
                                    "'d' Drop item";
                            }
                            else{                                                   //undefined
                                Item gear = selectedItem as Item;
                                extraInfoLabel.Text = gear.name + "\n\n" +
                                    "'c' Close view\n" +
                                    "'d' Drop item";
                            }

                        }

                        break;
                    }
                case EXTRA_GEAR: {          //showing gearlist
                    currentExtraMode = EXTRA_GEAR;
                    List<Entity> stuff = game.player.inventory;
                    extraInfoLabel.Text = "EQUIPMENT\n\n";

                    for (int i = 0; i < stuff.Count; i++ )
                    {
                        Item item = stuff[i] as Item;

                        String dontWorryAboutThisVar = "";
                        if (game.logics.checkIfItemIsEquipped(item, false, out dontWorryAboutThisVar))
                        {
                            extraInfoLabel.Text += String.Format("{0}) *{1}\n", i, item.name);
                        }
                        else
                        {
                            extraInfoLabel.Text += String.Format("{0}) {1}\n", i, item.name);
                        }

                    }

                    extraInfoLabel.Text += "\n\n'c' Close view";
                    
                    break; }
            }

        }

        public void Canvas_Paint(PaintEventArgs e)
        {
            e.Graphics.Clear(Color.Black);

            if (WindowMode == 0)   //dungeon mode
            {
                char[,] currRoom = game.World.getCurrentRoom().getLab();
                Labyrinth lab = game.World.getCurrentRoom();

                foreach (Entity ent in lab.floorList)     //floor draw
                {
                    e.Graphics.DrawImage(getResource(ent.symbol), ent.xPos * 32, ent.yPos * 32, 32, 32);    

                }

                foreach (Entity ent in lab.architectureList)     //architecture draw
                {
                    e.Graphics.DrawImage(getResource(ent.symbol), ent.xPos * 32, ent.yPos * 32, 32, 32);

                }

                foreach (Entity ent in lab.groundObjects)       //ground objects
                {
                    e.Graphics.DrawImage(getResource(ent.symbol), ent.xPos * 32, ent.yPos * 32, 32, 32);
                }


                //TODO items in dungeon

                foreach (Entity ent in lab.creatures)        //creatures
                {

                    e.Graphics.DrawImage(getResource(ent.symbol), ent.xPos * 32, ent.yPos * 32, 32, 32);

                }

                //TEXT MESSAGES
                int whereToStartText = currRoom.GetLength(0) * 32 + Form1.NEWLINE;
                //Player health, stats
                e.Graphics.DrawString(String.Format("{0} HP: {1}, Depth: {2}", game.player.name, game.player.getHealth(), game.currentWorld+1), 
                    messageFont, messageBrush, new PointF(Form1.MARGIN, whereToStartText));
                //Action messages, like combat etc
                e.Graphics.DrawString(container.Message, messageFont, messageBrush, new PointF(Form1.MARGIN, whereToStartText + Form1.NEWLINE + Form1.MARGIN));

                //below for animation
                int animationListSize = game.World.getCurrentRoom().animationList.Count;

                if (animationListSize > 0)
                {
                    List<Animation> aniList = game.World.getCurrentRoom().animationList;
                    for (int i = animationListSize - 1; i >= 0; i--)
                    {
                        Animation currentAni = aniList[i];
                        if (currentAni.Finished)
                        {
                            aniList.Remove(currentAni);
                            
                        }
                        else
                        {
                            e.Graphics.DrawImage(currentAni.bmp, currentAni.xPos * 32, currentAni.yPos * 32, currentAni.sizeX, currentAni.sizeY);
                        }
                    }
                }
            }
            else if (WindowMode == 1)  //map mode
            {
                Labyrinth[,] wList = game.World.WorldList;

                for (int y = 0; y < wList.GetLength(0); y++)
                {
                    for (int x = 0; x < wList.GetLength(1); x++)
                    {
                        if (wList[y, x] != null)
                        {
                            Labyrinth lab = wList[y, x];
                            String[] dL = lab.doorPositionsForMap;
                            bool thisOne = false;
                            ImageAttributes currentAttribs = defaultAttribs;
                            if (lab.Equals(game.World.getCurrentRoom()))        //current room should have different tint. TODO make explored rooms different tint
                            {
                                currentAttribs = attribsCurrentRoomOnMap;
                                thisOne = true;
                            }

                            /*
                             *Graphics.DrawImage Method (Image, Rectangle, Int32, Int32, Int32, Int32, GraphicsUnit, ImageAttributes) 
                             */

                            if (dL.Contains(Labyrinth.DOOR_POS_E) && !dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && !dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_ne, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //NE
                            else if (dL.Contains(Labyrinth.DOOR_POS_E) && !dL.Contains(Labyrinth.DOOR_POS_W) && !dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_se, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //SE
                            else if (dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && !dL.Contains(Labyrinth.DOOR_POS_N) && !dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_ew, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //EW
                            else if (dL.Contains(Labyrinth.DOOR_POS_E) && !dL.Contains(Labyrinth.DOOR_POS_W) && !dL.Contains(Labyrinth.DOOR_POS_N) && !dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_e, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //E
                            else if (!dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && !dL.Contains(Labyrinth.DOOR_POS_N) && !dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_w, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //W
                            else if (!dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && !dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_nw, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //NW
                            else if (!dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && !dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_sw, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //SW
                            else if (!dL.Contains(Labyrinth.DOOR_POS_E) && !dL.Contains(Labyrinth.DOOR_POS_W) && !dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_s, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //S
                            else if (!dL.Contains(Labyrinth.DOOR_POS_E) && !dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && !dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_n, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);      //N
                            else if (!dL.Contains(Labyrinth.DOOR_POS_E) && !dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_ns, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //NS
                            else if (dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_all, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);      //ALL
                            else if (!dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_nws, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);      //NWS
                            else if (dL.Contains(Labyrinth.DOOR_POS_E) && !dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_nes, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);      //NES
                            else if (dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && !dL.Contains(Labyrinth.DOOR_POS_N) && dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_swe, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);     //SWE
                            else if (dL.Contains(Labyrinth.DOOR_POS_E) && dL.Contains(Labyrinth.DOOR_POS_W) && dL.Contains(Labyrinth.DOOR_POS_N) && !dL.Contains(Labyrinth.DOOR_POS_S))
                                e.Graphics.DrawImage(Properties.Resources.map_nwe, new Rectangle(x * 16, y * 16, 16, 16), 0, 0, 16, 16, GraphicsUnit.Pixel, currentAttribs);       //NWE
                        }
                    }
                }
            }
        }

        //TODO
        public Bitmap getMapBmp(String type)
        {
            return null;
        }

        /// <summary>
        /// Returning bitmaps based on entity ascii
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public Bitmap getResource(char c)
        { 

            if (c == helper.getAscii(Helper.DOOR_ASCII))
            {
                return Properties.Resources.door;
            }
            else if (c == helper.getAscii(Helper.PLAYER_ASCII))
            {
                return Properties.Resources.player;
            }
            else if (c == helper.getAscii(Helper.WALL_ASCII))
            {
                return Properties.Resources.wall;
            }
            else if (c == helper.getAscii(Helper.FLOOR_ASCII))
            {
                return Properties.Resources.floor;
            }
            else if (c == helper.getAscii(Helper.KOBOLD_ASCII))
            {
                return Properties.Resources.kobold;
            }
            else if (c == helper.getAscii(Helper.LOOTBAG_ASCII))        //only lootbag is in for items/loot
            {
                return Properties.Resources.bag;
            }
            else if (c == helper.getAscii(Helper.STAIRSUP_ASCII))
            {
                return Properties.Resources.stairsup;
            }
            else if (c == helper.getAscii(Helper.STAIRSDOWN_ASCII))
            {
                return Properties.Resources.stairsdown;
            }
            return null;
        }

        /// <summary>
        /// player drops an item in this location
        /// </summary>
        /// <returns></returns>
        public bool dropItemWhere()
        {
            if (currentExtraMode == EXTRA_GEAR_STATS)
            {
                game.logics.playerDropItem();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles keyboard input
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Door doorObject = null;

            if (!Game.RUN)  //if player is dead
            {
                return true;
            }

            //mapmode
            if (keyData == Keys.M)
            {
                if (WindowMode == 0)
                {
                    WindowMode = 1;
                    displayInExtraColumn(EXTRA_MAP, null);
                    canvas.Refresh();
                }
                else
                {
                    WindowMode = 0;
                    displayInExtraColumn(EXTRA_NONE, null);
                    canvas.Refresh();
                }

                return true;
            }

            //closing extraview
            if (keyData == Keys.C)
            {
                if (WindowMode == 1)
                {
                    WindowMode = 0;
                    displayInExtraColumn(EXTRA_NONE, null);
                    canvas.Refresh();
                }
                displayInExtraColumn(EXTRA_NONE,null);
                return true;
            }


            //these controls only work in windowed mode
            if (WindowMode == 0){

                //number keys. getNumberPressed will check current mode (inventory etc). 48 == 0, 57 == 9
                if ((char)keyData >= 48 && (char)keyData <= 57)
                {
                    helper.debugMess("pressed " + (char)keyData);

                    switch (keyData)
                    {
                        case Keys.D0:
                            {
                                if (getNumberPressed(0))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D1:
                            {
                                if (getNumberPressed(1))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D2:
                            {
                                if (getNumberPressed(2))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D3:
                            {
                                if (getNumberPressed(3))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D4:
                            {
                                if (getNumberPressed(4))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D5:
                            {
                                if (getNumberPressed(5))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D6:
                            {
                                if (getNumberPressed(6))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D7:
                            {
                                if (getNumberPressed(7))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D8:
                            {
                                if (getNumberPressed(8))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                        case Keys.D9:
                            {
                                if (getNumberPressed(9))
                                {
                                    game.logics.moveActors();
                                    canvas.Refresh();
                                    return true;
                                }
                                return true;
                            }
                    }
                }


                //angle brackets
                if(keyData == (Keys.OemBackslash | Keys.Shift)){
                    Game.Debug_Mess("> pressed");

                    //if () TODO need to check where player is

                    if (game.switchLabDown())
                    {
                        canvas.Refresh();
                    }
                    return true;   
                }
                if (keyData == Keys.OemBackslash)
                {
                    Game.Debug_Mess("< pressed");
                    if (game.switchLabUp())
                    {
                        canvas.Refresh();
                    }
                    return true;
                }
                

                //inventory
                if (keyData == Keys.I)
                {
                    displayInExtraColumn(EXTRA_GEAR, null);
                    return true;
                }

                //if in gear stats, pressing u will unequip
                if (keyData == Keys.U && currentExtraMode== EXTRA_GEAR_STATS)
                {

                    if (currentGearStats == Game.GEAR_STATS_POTION)
                    {
                        game.logics.useItem(game.player.currentlySelectedItem);
                        game.logics.moveActors();
                        canvas.Refresh();
                        displayInExtraColumn(EXTRA_GEAR, null);
                        return true;
                    }
                    else if (currentGearStats == Game.GEAR_STATS_EQUIPPED)
                    {
                        string dontWorryMan = "";
                        game.logics.checkIfItemIsEquipped(game.player.currentlySelectedItem, true, out dontWorryMan);
                        game.logics.moveActors();
                        canvas.Refresh();
                        displayInExtraColumn(EXTRA_GEAR, null);
                        return true;
                    }
                    else
                    {
                        return true;
                    }

                }

                //if in gear stats, pressing e will equip
                if (keyData == Keys.E && currentExtraMode == EXTRA_GEAR_STATS)
                {
                    if (currentGearStats == Game.GEAR_STATS_EQUIPPABLE)
                    {
                        string dontWorryMang = "";
                        game.logics.checkIfItemCanBeEquipped(game.player.currentlySelectedItem, true,out dontWorryMang); //does not take into consideration if item is already equipped
                        game.logics.moveActors();
                        canvas.Refresh();
                        displayInExtraColumn(EXTRA_GEAR, null);
                        return true;
                    }
                    return true;
                }

                //dropping items
                if (keyData == Keys.D)
                {
                    if (dropItemWhere())
                    {
                        game.logics.moveActors();
                        canvas.Refresh();
                        displayInExtraColumn(EXTRA_GEAR, null);
                        
                    }
                    return true;
                }


                //pickup items
                if (keyData == Keys.P)
                {
                    Entity gObj = null;
                    if (game.World.getCurrentRoom().checkIfGroundObject(game.player.xPos, game.player.yPos, out gObj))
                    {
                        game.player.inventory.Add(gObj);
                        game.World.getCurrentRoom().removeGroundObject(gObj);
                        if (currentExtraMode == EXTRA_GEAR)     //updating equipment view if present
                            displayInExtraColumn(EXTRA_GEAR,null);
                        if (Game.DEBUG)
                        {
                            foreach(Item item in game.player.inventory){
                                Console.Write(item.name + ", ");
                            }
                            Console.WriteLine();
                        }

                    }
                    game.logics.moveActors();
                    canvas.Refresh();
                }





                //capture up arrow key
                if (keyData == Keys.Up || keyData == Keys.NumPad8)
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);
                    if (Game.DEBUG)
                        Console.WriteLine("door state is " + door);

                    if (door == -1)
                    {
                        game.logics.moveUp();
                        game.logics.moveActors();
                    
                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 0)
                        {
                            //TODO need to stop and release timers here as well?
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveUp();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }

                    }
                }

                else if (keyData == Keys.Down || keyData == Keys.NumPad2)
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);

                    if (door == -1)
                    {
                        game.logics.moveDown();
                        game.logics.moveActors();

                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 2)
                        {
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveDown();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }
                    }

                }
                else if (keyData == Keys.Left || keyData == Keys.NumPad4)
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);

                    if (door == -1)
                    {
                        game.logics.moveLeft();
                        game.logics.moveActors();

                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 1)
                        {
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveLeft();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }
                    }
                }

                else if (keyData == Keys.NumPad7)    //left up
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);

                    if (door == -1)
                    {
                        game.logics.moveLeftUp();
                        game.logics.moveActors();

                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 1)
                        {
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveLeft();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }
                    }
                }
                else if (keyData == Keys.NumPad9)   //right up
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);

                    if (door == -1)
                    {
                        game.logics.moveRightUp();
                        game.logics.moveActors();

                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 1)
                        {
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveLeft();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }
                    }
                }
                else if (keyData == Keys.NumPad1)   //left down
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);

                    if (door == -1)
                    {
                        game.logics.moveLeftDown();
                        game.logics.moveActors();

                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 1)
                        {
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveLeftDown();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }
                    }
                }
                else if (keyData == Keys.NumPad3)   //right down
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);

                    if (door == -1)
                    {
                        game.logics.moveRightDown();
                        game.logics.moveActors();

                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 1)
                        {
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveLeft();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }
                    }
                }
                else if (keyData == Keys.Right || keyData == Keys.NumPad6)
                {
                    container.clearMessages();
                    int door = game.logics.checkIfOnDoorTile(out doorObject);

                    if (door == -1)
                    {
                        game.logics.moveRight();
                        game.logics.moveActors();

                        game.player.debugInfo();
                        if (Game.GAME_MODE == 0)
                            game.World.getCurrentRoom().updateLabyrinth();
                        game.World.getCurrentRoom().printLabyrinth();
                        canvas.Refresh();
                        return true;
                    }
                    else
                    {
                        if (door == 3)
                        {
                            game.World.getCurrentRoom().animationList.Clear();
                            game.logics.doorMovement(doorObject);
                            canvas.Refresh();
                            return true;
                        }
                        else
                        {
                            game.logics.moveRight();
                            game.logics.moveActors();

                            game.player.debugInfo();
                            if (Game.GAME_MODE == 0)
                                game.World.getCurrentRoom().updateLabyrinth();
                            game.World.getCurrentRoom().printLabyrinth();
                            canvas.Refresh();
                            return true;
                        }
                    }

                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        /// <summary>
        /// Checking which floortile the player is looking at, useful for aoe-attacks, like grenades
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Entity getTilePosition()
        {
            Point p = canvas.PointToClient(Cursor.Position);        //gets mouse position and translates into canvas
            MouseHitBoxRect.X = p.X;
            MouseHitBoxRect.Y = p.Y;

            foreach(Entity floor in game.World.getCurrentRoom().floorList){
                if (floor.hitBox.Contains(MouseHitBoxRect))
                {
                    Game.Debug_Mess(String.Format("Hit position {0},{1}", floor.xPos, floor.yPos));
                    return floor;
                }
            }
            return null;
        }

        /// <summary>
        /// returns the creature the mouse pointer points at
        /// </summary>
        /// <param name="p"></param>
        /// <param name="interact"></param>
        /// <returns></returns>
        public Entity getTile(Point p)
        {
            MouseHitBoxRect.X = p.X;
            MouseHitBoxRect.Y = p.Y;

            foreach (Entity ent in game.World.getCurrentRoom().creatures)
            {
                if (ent.hitBox.Contains(MouseHitBoxRect))
                {
                    ent.triggerHitBox();
                    /*if (Game.DEBUG)
                    {
                        Console.WriteLine("Hitbox triggered by " + ent.name);
                        
                    }*/
                    lookAtLabel.Text = container.LootAtThisMess;
                    return ent;

                }
            }
            lookAtLabel.Text = container.LootAtThisMess;
            return null;

        }

        /// <summary>
        /// Used for animations, should only update the canvas if there are any animations to show
        /// </summary>
        public void run()
        {

            if (WindowMode == 0)
            {
                lookAtLabel.Invoke(mousePos);       //not needed, run on main thread
            }
            else
                container.clearLookAt();

            if (WindowMode == 0 && game.World.getCurrentRoom().animationList.Count > 0)
            {

                //canvas.Invoke(redraw);      //not used, animationloop trigger method is run on main thread
                canvas.Refresh();
            }
            
                
        }

        /// <summary>
        /// used by delegate to get mouse pos and what the player is looking at. Currently only used for monsters
        /// </summary>
        private Entity getMousePos()
        {
            container.clearLookAt();
            Point p = canvas.PointToClient(Cursor.Position);        //gets mouse position and translates into canvas
            Entity whatULookingAt = getTile(p);
            if (Game.MOUSE_DEBUG)
                Console.WriteLine(String.Format("mouse pos x{0}, y{1}", p.X, p.Y));
            return whatULookingAt;
        }

        /// <summary>
        /// used by delegate to redraw canvas
        /// not used atm since animation loop trigger is run om main thread
        /// </summary>
        private void invokeRefresh()
        {
            canvas.Refresh();
        }
    }

    public class Canvas : UserControl
    {
        Form1 main;

        public Canvas(Form1 main)
        {
            this.main = main;
            this.SetStyle(
                System.Windows.Forms.ControlStyles.UserPaint |          //reduces flickering
                System.Windows.Forms.ControlStyles.AllPaintingInWmPaint |
                System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer,
                true);
        }

        //override OnPaint and call the method
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            main.Canvas_Paint(e);
        }
    }
}
