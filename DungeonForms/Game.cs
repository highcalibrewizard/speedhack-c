﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonForms;
using DungeonForms.entities;
using System.Drawing;


namespace DungeonForms
{
    
    /// <summary>
    /// The main class for the Game, holds references to other classes
    /// </summary>
    public class Game
    {
        public const bool DEBUG =true;    //debug will cause alot of extra processing...
        public const bool MOUSE_DEBUG = false;  //debugging mouse position
        Helper helper;

        public const String CURRENT_VERSION = "0.1.0";

        public const int NUM_WORLDS = 3;
        public World World
        {
            get { return worldList[currentWorld]; }
        }
        public World[] worldList = new World[Game.NUM_WORLDS];
        public int currentWorld;

        public static bool RUN = true;   //if player is alive or not (if input commands work)
        public const int GAME_MODE = 1; //0 = ascii, 1 is winforms. Ascii currently not supported
        public const int LOOT_CHANCE = 4;   //chance of loot appearing, used in random.Next(LOOT_CHANCE), so 5 is 20% chance
        public const int LOOT_ITEMS_PER_ROOM = 3;   //number of loot items max per room

        /// <summary>
        /// Loot rarity tiers for weight system. check Helper class.
        /// </summary>
        public const int LOOT_RARITY_COMMON = 1000;     //100% baseline
        public const int LOOT_RARITY_UNCOMMON = 800;    //80% of common
        public const int LOOT_RARITY_RARE = 100;        //10% of common
        public const int LOOT_RARITY_VERYRARE = 10;     //1% of common

        /// <summary>
        /// Only use controller in Console project
        /// </summary>
        public Controller controller;

        public GameLogic logics;

        public Player player;


        //The type of item shown in the screen of the currently selected item (currently only for forms application)
        public const string GEAR_STATS_EQUIPPED = "GearStats_equipped";    //signifies item that is currently equipped
        public const string GEAR_STATS_EQUIPPABLE = "GearStats_equippable"; //an item that is equippable but currently not
        public const string GEAR_STATS_POTION = "GearStats_potion";    //item that can be consumed

        public Game()
        {
            helper = Helper.getInstance();
            player = new Player(helper.getAscii(Helper.PLAYER_ASCII), "player", 1, 1, 20, 20);

            //Generating worlds
            for (int i = 0; i < Game.NUM_WORLDS; i++)
            {
                worldList[i] = new World();
            }

            worldList[currentWorld].positionPlayer(player);

            //DEBUG
            if (Game.DEBUG)
            {
                //world.getCurrentRoom().addGroundObject(new ThrowableOneHand(1,1,"dummythrowable",0,0));
                //world.getCurrentRoom().addGroundObject(new ThrowableTwoHand(1, 1, "dummythrowable2", 0, 0));
            }

            logics = new GameLogic(this,player);

            if (Game.GAME_MODE == 0)
            {
                worldList[currentWorld].getCurrentRoom().updateLabyrinth();
                worldList[currentWorld].getCurrentRoom().printLabyrinth();
                worldList[currentWorld].printWorld(); //should be under debug flag
            }

        }

        /// <summary>
        /// Going down in world list (using stairs)
        /// </summary>
        public bool switchLabDown()
        {

            Entity stairs = null;

            if (logics.checkIfOnStairs(out stairs, World.getCurrentRoom())){

                if (stairs is StairsDown) { 

                    if (currentWorld < worldList.GetLength(0) - 1)
                    {
                        World.getCurrentRoom().removeCreature(player);
                        currentWorld++;
                        player.xPos = 1;
                        player.yPos = 1;
                        World.getCurrentRoom().addCreature(player);
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Going up in world list (using stairs). assumes start pos of stair is x1 and y1
        /// </summary>
        public bool switchLabUp()
        {

            Entity stairs = null;

            if (logics.checkIfOnStairs(out stairs, World.getCurrentRoom())) {

                if (stairs is StairsUp) { 
                    if (player.xPos == 1 && player.yPos == 1)
                    {
                        if (currentWorld > 0)
                        {
                            World.getCurrentRoom().removeCreature(player);
                            currentWorld--;
                            Labyrinth lab = World.listOfAllCreatedRooms[World.endPointWorldNum];
                            player.xPos = World.endPointStairs.X;
                            player.yPos = World.endPointStairs.Y;
                            lab.addCreature(player);
                            return true;
                        }
                        return false;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// This only works on Console. You only need GameLogic not controller for forms project
        /// moveLeft(), moveDown() etc.
        /// </summary>
        public void initConsole()
        {
            controller = new Controller(logics);
            controller.beginLoop();
        }

        public static void Debug_Mess(String mess)
        {
            if (DEBUG)
            {
                Console.WriteLine(mess);
            }
        }

      
    }
}
