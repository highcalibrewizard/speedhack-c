﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonForms;
using DungeonForms.entities;

namespace DungeonForms
{
    public class GameLogic
    {
        public Game game;
        public Player player;
        /// <summary>
        /// Constructor
        /// </summary>
        public GameLogic(Game game, Player player)
        {
            this.player = player;
            this.game = game;
        }

        /// <summary>
        /// Moves all the monsters, targetting player
        /// </summary>
        public void moveActors()
        {
           
            
            Labyrinth lab = game.World.getCurrentRoom();
            foreach(Entity ent in lab.creatures)
            {
                if (!(ent is Player))
                {
                    //ent.getDistanceToTarget(player);
                    //ent.selfMoveCheck(player, this);

                    ent.startAIMovement(player, this);
                    
                    if (!Game.RUN)
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// player dropping an item... Also removes from slot if equipped
        /// </summary>
        public void playerDropItem()
        {
            player.currentlySelectedItem.moveObject(player.xPos, player.yPos);
            player.inventory.Remove(player.currentlySelectedItem);
            game.World.getCurrentRoom().addGroundObject(player.currentlySelectedItem);
            string dontBotherWithThisVar = "";
            checkIfItemIsEquipped(player.currentlySelectedItem,true, out dontBotherWithThisVar);
        }

        /// <summary>
        /// Player uses an item, like potion, that is then removed
        /// </summary>
        public void useItem(Item item)
        {
            player.inventory.Remove(item);
            item.useItem();
        }


        /// <summary>
        /// just checks all the equipment slots of player to see if a supplied item is equipped
        /// supply deleteItem if you want the item to be deleted from equipped slot (if player is dropping it, or unequipping (when unequip it will still be in inventory))
        /// </summary>
        /// <param name="itemToCheck"></param>
        public bool checkIfItemIsEquipped(Item itemToCheck, bool deleteItem, out string currGearStats)
        {

            if (itemToCheck.Equals(player.armor))
            {
                if (deleteItem)
                {
                    player.armor = null;
                }
                currGearStats = Game.GEAR_STATS_EQUIPPED;
                return true;
            }



            else if (itemToCheck.Equals(player.mainHand))
            {
                if (deleteItem)
                {
                    player.mainHand = null;
                }
                currGearStats = Game.GEAR_STATS_EQUIPPED;
                return true;
            }



            else if (itemToCheck.Equals(player.offHand))
            {
                if (deleteItem)
                {
                    player.offHand = null;
                }
                currGearStats = Game.GEAR_STATS_EQUIPPED;
                return true;
            }


            else if (itemToCheck.Equals(player.twoHand))
            {
                if (deleteItem)
                {
                    player.twoHand = null;
                }
                currGearStats = Game.GEAR_STATS_EQUIPPED;
                return true;
            }

            currGearStats = Game.GEAR_STATS_EQUIPPABLE;
               
            return false;
        }



        /// <summary>
        /// Checks if the currently selected item can be equipped, and if so handles it automatically if equipNow is set
        /// </summary>
        /// <returns></returns>
        public bool checkIfItemCanBeEquipped(Item itemToCheck, bool equipNow, out string currGearType)
        {
            if (itemToCheck is OffHand)
            {

                if (equipNow)
                {
                    player.offHand = itemToCheck;
                    player.twoHand = null;
                }
                currGearType = Game.GEAR_STATS_EQUIPPABLE;
                return true;
            }
            else if (itemToCheck is OneHanded)
            {

                if (equipNow)
                {
                    player.mainHand = itemToCheck;
                    player.twoHand = null;
                }
                currGearType = Game.GEAR_STATS_EQUIPPABLE;
                return true;
            }
            else if (itemToCheck is TwoHanded)
            {
  
                if (equipNow)
                {
                    player.twoHand = itemToCheck;
                    player.mainHand = null;
                    player.offHand = null;
                }
                currGearType = Game.GEAR_STATS_EQUIPPABLE;
                return true;
            }
            else if (itemToCheck is ArmorBody)
            {

                if (equipNow)
                {
                    player.armor = itemToCheck;
                }
                currGearType = Game.GEAR_STATS_EQUIPPABLE;
                return true;
            }
            else if (itemToCheck is Potion)
            {
                currGearType = Game.GEAR_STATS_POTION;
                return false;
            }

            currGearType = "";
            return false;
        }

        //For now only checking if walls, used by AI
        /// <summary>
        /// Only checks if walls, used by AI. Combine with checkIfCreature(xPos,yPos, out creature) for creature check
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool checkObstacle(int x, int y)
        {
            Entity arch = null;
            if (game.World.getCurrentRoom().checkIfArchitecture(x, y, out arch))
            {

                if (arch != null && arch is Wall)       //Wall
                {
                    Game.Debug_Mess("Wall in the way");
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// Returns if player is on stairs, returns those stairs, or null. used by Game-class
        /// </summary>
        /// <param name="stairs"></param>
        public bool checkIfOnStairs(out Entity stairs, Labyrinth currentLab)
        {
            stairs = null;
            foreach (Entity ent in currentLab.architectureList)
            {
                if (player.comparePositions(ent) && (ent is StairsDown || ent is StairsUp))
                {
                    Game.Debug_Mess("on stairs");
                    stairs = ent;
                    return true;
                }
            }
            Game.Debug_Mess("not on stairs");
            return false;
        }

        /// <summary>
        /// Checking if player is standing on door tile
        /// </summary>
        /// <returns>Returns room position of door. -1 no door, 0 N, 1 W, 2 S, 3 E</returns>
        public int checkIfOnDoorTile(out Door doorObject)
        {
            List<Entity> list = game.World.getCurrentRoom().architectureList;

            foreach(Entity ent in list) { 
                if (ent is Door)
                {
                    if (player.xPos == ent.xPos && player.yPos == ent.yPos)
                    {
                        Door door = ent as Door;
                        doorObject = door;
                        return door.Position;
                    }
                }
            }
            doorObject = null;
            return -1;
        }

        /// <summary>
        /// Checks collisions and moves player
        /// </summary>
        /// <param name="xMod">Distance to travel in x</param> 
        /// <param name="yMod">Distance to travel in y</param> 
        /// <param name="ent">The entity that wants to move</param> 
        public bool checkCollisions(int xMod, int yMod, Entity ent)
        {
            int futurePosX = ent.xPos + xMod;
            int futurePosY = ent.yPos + yMod;
            char[,] checkThisLab = game.World.getCurrentRoom().getLab();

            if (futurePosX >= checkThisLab.GetLength(1) || futurePosX < 0)    //out of bounds x
            {
                return true;
            }

            if (futurePosY >= checkThisLab.GetLength(0) || futurePosY < 0)    //out of bounds y
            {
                return true;
            }

            Entity creature = null;
            if (game.World.getCurrentRoom().checkIfCreature(futurePosX, futurePosY, out creature))        //TODO: this should not look like this
            {
                if (ent is Player)
                {
                    string attackType = player.getAttackType();
                    if (attackType != Entity.ATTACK_MELEE_ONEHAND && attackType != Entity.ATTACK_MELEE_TWOHAND)
                        player.combat(creature, this, Entity.ATTACK_UNARMED);
                    else
                        player.combat(creature, this, attackType);
                }

                return true;
            }

            Entity arch = null;
            if (game.World.getCurrentRoom().checkIfArchitecture(futurePosX, futurePosY, out arch))
            {

                if (arch != null && arch is Wall)       //Wall
                {
                    Game.Debug_Mess("Wall in the way");
                    return true;
                }
            }

            ent.moveObject(futurePosX, futurePosY);
            return false;
        }

        /// <summary>
        /// Entering through door, use in conjuction with CheckIfOnDoorTile()
        /// </summary>
        public void doorMovement(Door tmpDoor)
        {
            Game.Debug_Mess("Entered door");
            game.World.moveTo(tmpDoor, player);

            //Debug shit
            game.World.getCurrentRoom().updateLabyrinth();
            game.World.getCurrentRoom().printLabyrinth();

            if (game.World.getCurrentRoom().creatures.Contains(player))
            {
                Game.Debug_Mess(String.Format("player location is x{0} y{1} ", player.xPos, player.yPos));
            }
            else
            {
                Game.Debug_Mess("player gone");
            }
            return;
        }

        public void moveLeft()
        {
            checkCollisions(-1, 0,player);

        }

        public void moveLeftUp()
        {
            checkCollisions(-1, -1, player);
        }
        public void moveLeftDown()
        {
            checkCollisions(-1, 1, player);
        }


        public void moveRight()
        {
            checkCollisions(1, 0, player);
        }
        public void moveRightUp()
        {
            checkCollisions(1, -1, player);
        }
        public void moveRightDown()
        {
            checkCollisions(1, 1, player);
        }

        public void moveUp()
        {
            checkCollisions(0, -1, player);
        }

        public void moveDown()
        {
            checkCollisions(0, 1, player);
        }
    }
}
