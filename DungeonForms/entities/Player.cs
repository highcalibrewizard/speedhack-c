﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{
    public class Player : Creature
    {

        
        /// <summary>
        /// The game needs a reference to the currently selected item, for inventory management
        /// </summary>
        public Item currentlySelectedItem;

        public Player(char symbol, string name, int xPos, int yPos, int health, int maxHealth): base(xPos,yPos,symbol, name, health, maxHealth)
        {

        }
    }
}
