﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{
    public class Kobold : Creature
    {
        public Kobold(int xPos, int yPos, char symbol, string name, int health, int maxHealth, int maxSpeed = 1) : base(xPos, yPos, symbol, name, health, maxHealth, maxSpeed)
        {

        }
    }
}
