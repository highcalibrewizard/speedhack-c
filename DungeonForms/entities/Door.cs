﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{
    public class Door : DungeonArchitecture
    {
        /// <summary>
        /// Which other room this door leads to
        /// </summary>
        private int[] connectedToRoomIndex;
        private int position;

        /// <summary>
        /// 0 = north, west = 1, south = 2, east = 3
        /// </summary>
        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public int RoomConnectionRow {
            get { return connectedToRoomIndex[0]; }
        }

        public int RoomConnectionCol
        {
            get { return connectedToRoomIndex[1]; }
        }

        /// <summary>
        /// Placeholder for locked doors
        /// </summary>
        public bool LockStatus;

        public Door(int xPos, int yPos, int[] connectedToRoomIndex, int position, char symbol, bool locked = false) : base(symbol,xPos,yPos)
        {
            this.LockStatus = locked;
            this.connectedToRoomIndex = connectedToRoomIndex;
            this.position = position;
        }
    }
}
