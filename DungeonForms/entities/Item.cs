﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{
    /// <summary>
    /// Holds a list of all items on the ground, if several
    /// THIS IS NOT USED ATM. Not sure if LootBag is needed at all.
    /// </summary>
    /*public class LootBag : Entity
    {
        public List<Item> items = new List<Item>();

        public LootBag(int xPos, int yPos, string name = "Loot bag", char symbol = 'b') : base(symbol,name, xPos, yPos)
        {

        }

        /// <summary>
        /// Adds all the items in the lootbag to the inventory
        /// </summary>
        /// <param name="listToPutIn"></param>
        public void getLootContent(List<Entity> listToPutIn)
        {
            listToPutIn.AddRange(items);
        }

        public void addLootItem(Item lootItem)
        {
            items.Add(lootItem);
        }
    }*/

    /// <summary>
    /// A singular item on ground or in inventory
    /// </summary>
    public class Item : Entity
    {
        /// <summary>
        /// used by loot generator
        /// </summary>
        private int floorWeight;
        private int ceilWeight;


        /// <summary>
        /// Compare to FloorW when grabbing loot
        /// </summary>
        public int FloorW
        {
            get
            {
                return floorWeight;
            }
        }

        /// <summary>
        /// The ceiling index of weight. Use this when grabbing appropriate loot
        /// </summary>
        public int CeilCompareToThis
        {
            get
            {
                return ceilWeight;
            }
        }

        /// <summary>
        /// Use CeilW when adding to loottable
        /// </summary>
        public int CeilW
        {
            get
            {
                return ceilWeight+1;
            }
        }

        public Item(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(symbol, name, xPos, yPos)
        {

            calculateWeight(sum, weight);

        }

        /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public Item(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis.symbol,cloneThis.name,xPosNew,yPosNew)
        {
            name = cloneThis.name;
            symbol = cloneThis.symbol;

        }

        private void calculateWeight(int sum, int weight)
        {
            floorWeight = sum;
            ceilWeight = sum + weight;
        }

        public void useItem()
        {
            Game.Debug_Mess("item been used");
        }
    }


}
