﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{
    public class Wall : DungeonArchitecture
    {

        public Wall(int xPos, int yPos, char symbol) : base(symbol, xPos, yPos)
        {

        }
    }
}
