﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonForms;
using System.Drawing;

namespace DungeonForms.entities
{
    /// <summary>
    /// The base class for all displayed objects
    /// </summary>
    public abstract class Entity
    {
        public int xPos;
        public int yPos;
        public char symbol;
        public String name;
        public int bestAIValue;
        private int defaultAIValue = 100;
        public const int AInumberOfPassesMax = 1;
        public MessageContainer messageContainer;
        public RectangleF hitBox;

        /// <summary>
        /// these are for checking what weapon the creature is equipped with
        /// </summary>
        public const string ATTACK_MELEE_TWOHAND = "twohand";
        public const string ATTACK_MELEE_ONEHAND = "onehand";
        public const string ATTACK_RANGED_ONEHAND = "ranged_onehand";
        public const string ATTACK_RANGED_TWOHAND = "ranged_twohand";
        public const string ATTACK_THROWABLE_ONEHAND = "throwable_onehand";
        public const string ATTACK_THROWABLE_TWOHAND = "throwable_twohand";
        public const string ATTACK_UNARMED = "unarmed";
        public const string ATTACK_UNDEFINED = "undefined";
        public const string ATTACK_SPELL_RANGED = "ranged_spell";


        /// <summary>
        /// Compares the x,y of another entity
        /// </summary>
        /// <param name="compareWith"></param>
        /// <returns></returns>
        public bool comparePositions(Entity compareWith)
        {
            if (compareWith.xPos == xPos && compareWith.yPos == yPos)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// returns the type of attack that is possible by a Creature type, as string. if undefined no attack is possible
        /// This is used by player
        /// </summary>
        /// <returns></returns>
        public string getAttackType()
        {
            if (this is Creature)
            {
                Creature currentCreature = this as Creature;

                if (currentCreature.currentSpell != null)               //spells are checked first, after casting with 'c' set currentspell to null
                {
                    if (currentCreature.currentSpell is SpellAttackRanged)
                    {
                        return ATTACK_SPELL_RANGED;
                    }
                }

                else if (currentCreature.mainHand != null)
                {
                    if (currentCreature.mainHand is ThrowableOneHand)
                    {
                        return ATTACK_THROWABLE_ONEHAND;
                    }
                    else if (currentCreature.mainHand is RangedOneHand)
                    {
                        return ATTACK_RANGED_ONEHAND;
                    }
                    else if (currentCreature.mainHand is OneHanded)
                    {
                        return ATTACK_MELEE_ONEHAND;
                    }
                    
                }
                else if (currentCreature.twoHand != null)
                {
                    if (currentCreature.twoHand is ThrowableTwoHand)
                    {
                        return ATTACK_THROWABLE_TWOHAND;
                    }
                    else if (currentCreature.twoHand is RangedTwoHand)
                    {
                        return ATTACK_RANGED_TWOHAND;
                    }
                    else if (currentCreature.twoHand is TwoHanded)
                    {
                        return ATTACK_MELEE_TWOHAND;
                    }
                }
                else
                {
                    return ATTACK_UNARMED;
                }
            }
            return ATTACK_UNDEFINED;
        }

        public Entity(char character, String name,int xPos = 0, int yPos = 0)
        {
            this.name = name;
            this.symbol = character;
            this.xPos = xPos;
            this.yPos = yPos;
            messageContainer = MessageContainer.getContainer();
            if (Game.GAME_MODE == 1)
                setHitBox();
        }



        private void setHitBox(){
            hitBox = new RectangleF(xPos * Form1.TILE_SIZE, yPos * Form1.TILE_SIZE, Form1.TILE_SIZE, Form1.TILE_SIZE);
        }


        /// <summary>
        /// Doing an aoe attack on position x,y with radius z, where radius is >= 1. 1 is one tile extra effect, not counting impact tile
        /// Returns an array of all creatures impacted
        /// </summary>
        /// <param name="logic"></param>
        /// <param name="radius"></param>
        /// <param name="position"></param>
        public List<Entity> aoeAttack(GameLogic logic, int radius, int[] position)
        {
            char[,] room = logic.game.World.getCurrentRoom().getLab();
            List<Entity> impactList = new List<Entity>();

            for (int x = position[0]-radius; x <= position[0]+radius; x++)
            {
                for (int y = position[1]-radius; y <= position[1]+radius; y++)
                {
                    if (x >= 0 && x < room.GetLength(1) && y >= 0 && y < room.GetLength(0))
                    {
                        Game.Debug_Mess(String.Format("AOE Hit tile x{0},y{1}",x,y));
                        Entity impactedCreature = null;
                        logic.game.World.getCurrentRoom().checkIfCreature(x, y, out impactedCreature);
                        if (impactedCreature != null)
                            impactList.Add(impactedCreature);
                    }
                }
            }

            return impactList;
        }

        private void updateHitBox()
        {
            hitBox.X = xPos * Form1.TILE_SIZE;
            hitBox.Y = yPos * Form1.TILE_SIZE;
        }

        public void triggerHitBox(){
            messageContainer.addLookAtMess(name);
        }

        /// <summary>
        /// Starting count is AInumberOfPassesMax, starting index is current entity position: x,y
        /// The method does 5 passes by default and saves the smallest distance to player
        /// count keeps track of depth
        /// </summary>
        /// <returns></returns>
        private void AIMovement(int count, int xPos, int yPos, Entity otherEnt, GameLogic logic)
        {
            int distance = 0;
            Entity creature = null;
            if (count < 0 || logic.checkObstacle(xPos,yPos) || 
                logic.game.World.getCurrentRoom().checkIfCreature(xPos,yPos, out creature))    //add in more checks in checkObstacle if needed
            {
                if (creature is Player)
                {
                    distance = getDistanceToTarget(otherEnt.xPos,otherEnt.yPos, xPos, yPos);

                    if (distance < bestAIValue)
                    {
                        bestAIValue = distance;
                    }
                }
                return;
            }

            distance = getDistanceToTarget(otherEnt.xPos,otherEnt.yPos, xPos, yPos);

            if (distance < bestAIValue)
            {
                bestAIValue = distance;
            }

            AIMovement(count - 1, xPos-1, yPos, otherEnt, logic);  //check left move
            AIMovement(count - 1, xPos + 1, yPos, otherEnt, logic);  //check right move
            AIMovement(count - 1, xPos, yPos - 1, otherEnt, logic); //check move up
            AIMovement(count - 1, xPos, yPos + 1, otherEnt, logic);  //check  move down

            AIMovement(count - 1, xPos - 1, yPos-1, otherEnt, logic);  //check left up move
            AIMovement(count - 1, xPos - 1, yPos+1, otherEnt, logic);  //check left down move
            AIMovement(count - 1, xPos + 1, yPos-1, otherEnt, logic);   //check right up move
            AIMovement(count - 1, xPos + 1, yPos +1, otherEnt, logic);   //check right down move
        }

        /// <summary>
        /// For example a ranged attack, needing clear line of sight. 
        /// </summary>
        /// <param name="targetEntity"></param>
        public bool rangedInteraction(Entity targetEntity, GameLogic logic, string attackType)
        {
            if (checkLineOfSight(targetEntity.xPos,targetEntity.yPos, xPos, yPos, logic))
            {
                combat(targetEntity, logic,attackType);
                return true;
            }
            else
            {
                messageContainer.clearMessages();
                messageContainer.addMessage("Something in the way");
                return false;
            }

        }

        /// <summary>
        /// LoS for AoE-effects
        /// </summary>
        /// <param name="targetX"></param>
        /// <param name="targetY"></param>
        /// <param name="currentX"></param>
        /// <param name="currentY"></param>
        /// <param name="logic"></param>
        /// <param name="continueThis"></param>
        /// <returns></returns>
        public bool checkLoSAoE(int targetX, int targetY, int currentX, int currentY, GameLogic logic)
        {
            //if (!continueThis)
            //    return false;

            if (logic.checkObstacle(currentX, currentY))  //if obstacle, return false
            {
                return false;
            }

            //int xDiff = targetX - currentX;
            //int yDiff = targetY - currentY;

            int xMod = getDiff(targetX, currentX);
            int yMod = getDiff(targetY, currentY);


            if (xMod == 0 && yMod == 0)   //current point is on target point, ending
            {
                return true;
            }

            int tryThisX = currentX + xMod;
            int tryThisY = currentY + yMod;

            Game.Debug_Mess(String.Format("x{0},y{1},xMod{2},yMod{3}", tryThisX, tryThisY, xMod, yMod));

            return checkLoSAoE(targetX, targetY, tryThisX, tryThisY, logic);
        }

        /// <summary>
        /// checking LoS by recursion. continueThis = true will continue the recursion.
        /// </summary>
        /// <param name="targetX"></param>
        /// <param name="targetY"></param>
        /// <param name="currentX"></param>
        /// <param name="currentY"></param>
        /// <param name="logic"></param>
        /// <returns></returns>
        public bool checkLineOfSight(int targetX, int targetY, int currentX, int currentY, GameLogic logic)
        {
            //if(!continueThis)
            //    return false;

            if (logic.checkObstacle(currentX, currentY))  //if obstacle, return false
            {
                return false;
            }

            //int xDiff = targetX - currentX;
            //int yDiff = targetY - currentY;

            //if (getDistanceToTarget(targetX,targetY,currentX,currentY) == 1)   //target is on next tile, return true
            //{
            //    return true;
            //}

            int xMod = getDiff(targetX,currentX);
            int yMod = getDiff(targetY,currentY);

            if (xMod == 0 && yMod == 0)
            {
                return true;
            }

            int tryThisX = currentX + xMod;
            int tryThisY = currentY + yMod;
            
            Game.Debug_Mess(String.Format("x{0},y{1},xMod{2},yMod{3}",tryThisX,tryThisY,xMod,yMod));

            return checkLineOfSight(targetX,targetY, tryThisX, tryThisY,logic);
        }

        /// <summary>
        /// returns negative or positive move, used in check LoS. Secondnum is for the projectile
        /// </summary>
        /// <returns></returns>
        public int getDiff(int targetNum, int currentNum)
        {
            if (targetNum > currentNum)
            {
                return 1;
            }
            else if (targetNum < currentNum)
            {
                return -1;
            }
            else if (targetNum == currentNum)
            {
                return 0;
            }
            return 0;
        }

        public void combat(Entity target,GameLogic logic, string attackType)
        {
            Creature attacker = this as Creature;
            Creature targetCreature = target as Creature;

            Game.Debug_Mess(name + " wants to use " + attackType);

            if (attackType == ATTACK_MELEE_ONEHAND)
            {
                
            }
            else if (attackType == ATTACK_MELEE_TWOHAND)
            {

            }
            else if (attackType == ATTACK_UNARMED)
            {

            }
            else if (attackType == ATTACK_RANGED_ONEHAND)
            {

            }
            else if (attackType == ATTACK_RANGED_TWOHAND)
            {

            }
            else if(attackType == ATTACK_SPELL_RANGED){

            }


            bool dead = targetCreature.takeDamage(attacker.getDamage());

            if (Game.GAME_MODE == 1)
            {
                Bitmap bmp = ResourceCollection.getResourceCollection().getBmp(ResourceCollection.SPLATTER);
                logic.game.World.getCurrentRoom().animationList.Add(new Animation(target.xPos,target.yPos,32,32,bmp,Animation.DURATION_SHORT));
            }

            messageContainer.addMessage(String.Format("{0} attacked {1} dealing {2}, hp left {3}",attacker.name,targetCreature.name,attacker.getDamage(),targetCreature.getHealth()));

            if (Game.DEBUG)
            {
                Console.WriteLine(String.Format("{0} attacked {1} dealing {2}, hp left {3}",attacker.name,targetCreature.name,attacker.getDamage(),targetCreature.getHealth()));
            }

            if (dead)
            {
                messageContainer.addMessage(String.Format("{0} destroyed", targetCreature.name));
                if (targetCreature is Player)
                {
                    Game.RUN = false;
                }
                logic.game.World.getCurrentRoom().removeCreature(targetCreature);
            }

            
        }

        /// <summary>
        /// decides the best move and then executes it
        /// </summary>
        /// <param name="otherEnt"></param>
        /// <param name="logic"></param>
        public void startAIMovement(Entity otherEnt, GameLogic logic)
        {
            Creature thisCreature = this as Creature;
            string attackType = thisCreature.getAttackType();
            
            //checks if speed allows creature to move, the method also resets and adds to speed if needed
            if (!thisCreature.checkIfSpeedAllowsMove())
            {
                return;
            }

            if (getDistanceToTarget(otherEnt.xPos, otherEnt.yPos, xPos, yPos) == 1)
            {
                //ATTACK 
                if (Game.DEBUG)
                    Console.WriteLine("Should attack player");
                combat(otherEnt as Creature,logic,attackType);
                return;
            }

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos - 1, yPos, otherEnt, logic);  //check left move
            int left = bestAIValue;

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos - 1, yPos-1, otherEnt, logic);  //check left up move
            int leftup = bestAIValue;

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos - 1, yPos+1, otherEnt, logic);  //check left down move
            int leftdown = bestAIValue;

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos + 1, yPos, otherEnt, logic);  //check right move
            int right = bestAIValue;

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos + 1, yPos-1, otherEnt, logic);  //check right up move
            int rightup = bestAIValue;

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos + 1, yPos+1, otherEnt, logic);  //check right down move
            int rightdown = bestAIValue;

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos, yPos - 1, otherEnt, logic); //check move up
            int up = bestAIValue;

            bestAIValue = defaultAIValue;
            AIMovement(AInumberOfPassesMax, xPos, yPos + 1, otherEnt, logic);  //check  move down
            int down = bestAIValue;

            if (Game.DEBUG)
                Console.WriteLine(String.Format("Left {0},Right {1},Top {2}, Down{3}", left, right, up, down));


            if(left == 100 && right == 100 && up == 100 && down == 100 && leftup == 100 && leftdown == 100 && 
                rightup == 100 && rightdown == 100)
            {
                if (Game.DEBUG)
                    Console.WriteLine("no movement possible");
                return;
            }

            Entity outCreature;

            if (left <= right && left <= up && left <= down
                && left <= leftdown && left <= leftup && left <= rightdown && left <= rightup)   //should move left
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move left");
                
                if (otherEnt.yPos < yPos)       //this enables diagonal movement (just for the cosmetic look). but also slows the game down "some"
                {
                    if (!logic.checkObstacle(xPos - 1, yPos - 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos-1, yPos-1, out outCreature))
                    {
                        moveObject(xPos - 1, yPos-1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos - 1, yPos); //should be safe to move here
                    }
                }
                else if (otherEnt.yPos > yPos)    //this enables diagonal movement
                {
                    if (!logic.checkObstacle(xPos - 1, yPos + 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos - 1, yPos + 1, out outCreature))
                    {
                        moveObject(xPos - 1, yPos + 1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos - 1, yPos); //should be safe to move here
                    }
                }
                else
                {
                    moveObject(xPos - 1, yPos); //should be safe to move here
                }
                    
                
            }
            else if (right <= left && right <= up && right <= down
                && right <= leftdown && right <= leftup && right <= rightdown && right <= rightup)   //should move right
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move right");

                if (otherEnt.yPos < yPos)       //this enables diagonal movement (just for the cosmetic look). but also slows the game down "some"
                {
                    if (!logic.checkObstacle(xPos + 1, yPos - 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos + 1, yPos - 1, out outCreature))
                    {
                        moveObject(xPos + 1, yPos - 1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos + 1, yPos); //should be safe to move here
                    }
                }
                else if (otherEnt.yPos > yPos)    //this enables diagonal movement
                {
                    if (!logic.checkObstacle(xPos + 1, yPos + 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos + 1, yPos + 1, out outCreature))
                    {
                        moveObject(xPos + 1, yPos + 1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos + 1, yPos); //should be safe to move here
                    }
                }
                else
                {
                    moveObject(xPos + 1, yPos); //should be safe to move here
                }
                
            }
            else if (up <= right && up <= left && up <= down
                && up <= leftdown && up <= leftup && up <= rightdown && up <= rightup)   //should move up
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move up");

                if (otherEnt.xPos > xPos)
                {
                    if (!logic.checkObstacle(xPos + 1, yPos - 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos + 1, yPos - 1, out outCreature))
                    {
                        moveObject(xPos+1, yPos - 1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos, yPos - 1); //should be safe to move here
                    }
                }
                else if(otherEnt.xPos < xPos){
                    if (!logic.checkObstacle(xPos - 1, yPos - 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos - 1, yPos - 1, out outCreature))
                    {
                        moveObject(xPos - 1, yPos - 1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos, yPos - 1); //should be safe to move here
                    }
                }
                else{
                    moveObject(xPos, yPos - 1); //should be safe to move here
                }
     
                
                
            }
            else if (down <= up && down <= left && down <= right
                && down <= leftdown && down <= leftup && down <= rightdown && down <= rightup)   //should move down
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move down");

                if (otherEnt.xPos > xPos)
                {
                    if (!logic.checkObstacle(xPos + 1, yPos + 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos + 1, yPos + 1, out outCreature))
                    {
                        moveObject(xPos + 1, yPos + 1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos, yPos + 1); //should be safe to move here
                    }
                }
                else if (otherEnt.xPos < xPos)
                {
                    if (!logic.checkObstacle(xPos - 1, yPos + 1) && !logic.game.World.getCurrentRoom().checkIfCreature(xPos - 1, yPos + 1, out outCreature))
                    {
                        moveObject(xPos - 1, yPos + 1); //should be safe to move here
                    }
                    else
                    {
                        moveObject(xPos, yPos + 1); //should be safe to move here
                    }
                }
                else
                {
                    moveObject(xPos, yPos + 1); //should be safe to move here
                }
            }

            else if (leftup <= up && leftup <= left && leftup <= right && leftup <= down
                && leftup <= leftdown && leftup <= rightdown && leftup <= rightup)   //should move left up
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move left up");
                //moveObject(xPos, yPos + 1);
                //logic.checkCollisions(-1, -1, this);
                /*Entity creature = null;
                if (logic.game.World.getCurrentRoom().checkIfCreature(xPos - 1, yPos-1, out creature))
                {
                    //attack player
                    if (Game.DEBUG)
                        Console.WriteLine("Should attack player");
                }
                else*/
                {
                    moveObject(xPos - 1, yPos-1); //should be safe to move here
                }
            }
            else if (leftdown <= up && leftdown <= left && leftdown <= right && leftdown <= down
                && leftdown <= leftup && leftdown <= rightdown && leftdown <= rightup)   //should move left down
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move left down");
                //moveObject(xPos, yPos + 1);
                //logic.checkCollisions(-1, 1, this);
                /*Entity creature = null;
                if (logic.game.World.getCurrentRoom().checkIfCreature(xPos - 1, yPos+1, out creature))
                {
                    //attack player
                    if (Game.DEBUG)
                        Console.WriteLine("Should attack player");
                }
                else*/
                {
                    moveObject(xPos - 1, yPos+1); //should be safe to move here
                }
            }
            else if (rightup <= up && rightup <= left && rightup <= right && rightup <= down
                && rightup <= leftdown && rightup <= rightdown && rightup <= leftup)   //should move right up
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move right up");
                //moveObject(xPos, yPos + 1);
                //logic.checkCollisions(1, -1, this);
                /*Entity creature = null;
                if (logic.game.World.getCurrentRoom().checkIfCreature(xPos + 1, yPos-1, out creature))
                {
                    //attack player
                    if (Game.DEBUG)
                        Console.WriteLine("Should attack player");
                }
                else*/
                {
                    moveObject(xPos + 1, yPos-1); //should be safe to move here
                }
            }

            else if (rightdown <= up && rightdown <= left && rightdown <= right && rightdown <= down
                && rightdown <= leftdown && rightdown <= rightup && rightdown <= leftup)   //should move right down
            {
                if (Game.DEBUG)
                    Console.WriteLine("Should move right down");
                //moveObject(xPos, yPos + 1);
                //logic.checkCollisions(1, 1, this);
                /*Entity creature = null;
                if (logic.game.World.getCurrentRoom().checkIfCreature(xPos + 1, yPos+1, out creature))
                {
                    //attack player
                    if (Game.DEBUG)
                        Console.WriteLine("Should attack player");
                }
                else*/
                {
                    moveObject(xPos + 1, yPos+1); //should be safe to move here
                }
            }



            //todo diagonal movement
            //todo collision handling
            //something else here
            //Todo should monster check old player position? that would be best
        }


        public int getDistanceToTarget(int otherX, int otherY, int xPos, int yPos)
        {
            int xDist = Math.Abs(xPos - otherX);
            int yDist = Math.Abs(yPos - otherY);
            int largestValue = Math.Max(xDist, yDist);
            return largestValue;
        }

        public void moveObject(int moveToX, int moveToY)
        {
            xPos = moveToX;
            yPos = moveToY;
            updateHitBox();
        }

        public void debugInfo()
        {
            Console.WriteLine(String.Format("name {0} x pos {1}, y pos {2}",name, xPos, yPos));
        }
    }
}
