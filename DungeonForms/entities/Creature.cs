﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonForms;
using DungeonForms.entities;

namespace DungeonForms.entities
{
    public class Creature : Entity
    {
        private int health;
        private int maxHealth;
        private int damage = 1;
        public int speed;                //the speed of the creature. 2 = move every second turn, 3= move every third turn. So speed 1 is max.
        public int currentSpeed = 1;    //starts at 1, adds +1 if no move possible, resets to 1 when moved

        /// <summary>
        /// The inventory of the player (mostly)
        /// </summary>
        public List<Entity> inventory = new List<Entity>();
        public Item mainHand;   //onehanded item equipped (weapon)
        public Item offHand;    //(shield) when twohanded is equipped, cant equip offhand, make sure twohander is unequipped when doing so.
        public Item twoHand;    //twohanded weapon
        public Item armor;      //body armor
        public Spell currentSpell;  //current spell. NOT YET IN USE

        /// <summary>
        /// This field is used by the AI. It determines what kind of attack this entity prefers to use. Remember to set this, and the
        /// appropriate equipment/spell when constructing monsters. Set it with the constants in Entity class
        /// </summary>
        public string ai_attack_preference;

        /// <summary>
        /// returns the ai preference of attack
        /// </summary>
        /// <returns></returns>
        public string getMonsterAttackPreference()
        {
            if (ai_attack_preference == null)
                return ATTACK_UNARMED;
            else
                return ai_attack_preference;
        }

        public Creature(int xPos, int yPos, char symbol, string name, int health, int maxHealth, int maxSpeed = 1) : base(symbol, name,yPos, xPos)
        {
            this.health = health;
            this.maxHealth = maxHealth;
            this.speed = maxSpeed;
        }

        /// <summary>
        /// checks and resets speed of creature. Do check before every action.
        /// </summary>
        /// <returns></returns>
        public bool checkIfSpeedAllowsMove()
        {
            if (speed == currentSpeed)
            {
                currentSpeed = 1;
                return true;
            }
            else
            {
                currentSpeed++;
                return false;
            }
        }

        /// <summary>
        /// Returning current health
        /// </summary>
        /// <returns></returns>
        public int getHealth()
        {
            return health;
        }

        /// <summary>
        /// Returning entities damage potential
        /// </summary>
        /// <returns></returns>
        public int getDamage()
        {
            return damage;
        }

        public bool takeDamage(int damage)
        {
            health -= damage;
            if (health <= 0)
            {
                return true;
            }
            return false;
        }

    }
}
