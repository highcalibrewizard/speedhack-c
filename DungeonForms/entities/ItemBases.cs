﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{
    /// <summary>
    /// Base class for all damaging items
    /// </summary>
    public class Weapon : Item
    {
        public Weapon(int xPos, int yPos, string name,int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

        /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public Weapon(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }


    /// <summary>
    /// Onehanded consumable throwable weapon
    /// </summary>
    public class ThrowableOneHand : OneHanded
    {
        public ThrowableOneHand(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

        /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public ThrowableOneHand(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Twohanded consumable throwable weapon
    /// </summary>
    public class ThrowableTwoHand : TwoHanded
    {
        public ThrowableTwoHand(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public ThrowableTwoHand(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Twohand ranged
    /// </summary>
    public class RangedTwoHand : TwoHanded
    {
        public RangedTwoHand(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }
                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public RangedTwoHand(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Onehand ranged
    /// </summary>
    public class RangedOneHand : OneHanded
    {
        public RangedOneHand(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public RangedOneHand(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Onehanded weapon
    /// </summary>
    public class OneHanded : Weapon
    {
        public OneHanded(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public OneHanded(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Twohanded weapon
    /// </summary>
    public class TwoHanded : Weapon
    {
        public TwoHanded(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public TwoHanded(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Shield
    /// </summary>
    public class OffHand : Item
    {
        public OffHand(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }
                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public OffHand(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Base class for all armor items
    /// </summary>
    public class Armor : Item
    {
        public Armor(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public Armor(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    public class ArmorBody : Armor
    {
        public ArmorBody(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }
                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public ArmorBody(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }

    /// <summary>
    /// Base consumable
    /// </summary>
    public class Potion : Item
    {
        public Potion(int xPos, int yPos, string name, int weight, int sum, char symbol = 'b')
            : base(xPos, yPos, name, weight, sum, symbol)
        {

        }

                /// <summary>
        /// When cloning an object,we need to implement this manually
        /// </summary>
        /// <param name="cloneThis"></param>
        public Potion(Item cloneThis, int xPosNew, int yPosNew): base(cloneThis,xPosNew,yPosNew)
        {

        }
    }
}
