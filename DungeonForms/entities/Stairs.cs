﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{


    class StairsDown : DungeonArchitecture
    {

        public StairsDown(int xPos, int yPos, char symbol='>',string name = "stairs down") 
            : base(symbol,xPos,yPos,name)
        {

        }
    }

    class StairsUp : DungeonArchitecture
    {
        public StairsUp(int xPos, int yPos, char symbol='<',string name = "stairs up") 
            : base(symbol,xPos,yPos,name)
        {

        }
    }
}
