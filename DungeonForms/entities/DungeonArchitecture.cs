﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms.entities
{
    public class DungeonArchitecture : Entity
    {

        public DungeonArchitecture(char symbol, int xPos, int yPos, String name="dungeon tile") : base(symbol, name,xPos,yPos)
        {

        }
    }
}
