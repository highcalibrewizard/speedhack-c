﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms
{
    public class Controller
    {

        GameLogic logics;


        /// <summary>
        /// Controller class for the Game, handles user input
        /// </summary>
        public Controller(GameLogic logics)
        {
            this.logics = logics;
        }

        /// <summary>
        /// Begins the while loop for the Console project
        /// </summary>
        public void beginLoop()
        {
            while (true)
            {
                doCommand();
                String input = Console.ReadLine();
                input = input.Trim();
                input = input.ToLower();

                switch (input)
                {
                    case "right": { logics.moveRight(); break; }
                    case "left": { logics.moveLeft(); break; }
                    case "up": { logics.moveUp(); break; }
                    case "down": { logics.moveDown(); break; }
                    default: { Console.WriteLine("Valid commands are right, left, up, down"); break; }
                }
            }
        }

        /// <summary>
        /// Prints the what to do command in console project
        /// </summary>
        private void doCommand()
        {
            Console.Write("Your command?:");
        }
    }
}
