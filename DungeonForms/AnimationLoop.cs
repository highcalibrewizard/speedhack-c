﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonForms
{
    /// <summary>
    /// A simple timer for displaying animations and what-not. used with winforms only
    /// </summary>
    public class AnimationLoop
    {
        public Timer timer;
        private Renderer renderer;

        public AnimationLoop(Renderer renderer)
        {
            this.renderer = renderer;
            timer = new Timer();
            timer.Interval = 1000 / 30;
            timer.Tick += new EventHandler(tick);
            timer.Start();
        }

        private void tick(object sender, EventArgs e)
        {
            renderer.run();
        }


        /// <summary>
        /// called by the dispose method from forms
        /// </summary>
        public void disableTimer()
        {
            timer.Stop();
            timer.Dispose();
        }
    }
}
