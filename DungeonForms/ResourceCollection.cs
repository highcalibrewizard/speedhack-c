﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms
{

    public class ResourceCollection
    {
        public const String SPLATTER = "splatter";
        private Dictionary<String, Bitmap> gfx = new Dictionary<string, Bitmap>();
        private static ResourceCollection collection = new ResourceCollection();

        /// <summary>
        /// Holding additional GFX for winforms, used to communicate with the rest of the code
        /// </summary>
        /// <param name="forms"></param>
        private ResourceCollection()
        {
        }

        public void AddToCollection(String key, Bitmap bmp)
        {
            gfx.Add(key, bmp);
        }

        /// <summary>
        /// Returns the current collection, might be empty if you havent called AddToCollection() first
        /// </summary>
        /// <returns></returns>
        public static ResourceCollection getResourceCollection(){
            return collection;
        }

        public Bitmap getBmp(String key)
        {
            if (!gfx.ContainsKey(key))
            {
                return null;
            }
            else
            {
                return gfx[key];
            }
        }
    }
}
