﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonForms;
using DungeonForms.entities;
using System.Drawing;

namespace DungeonForms
{
    public class World
    {

        private Labyrinth[,] worldList = new Labyrinth[22,22];
        public Labyrinth[,] WorldList
        {
            get { return worldList; }
        }
        private Helper helper = Helper.getInstance();

        /// <summary>
        /// This List is only for adding walls to created rooms, since it is faster to loop through non-nulls
        /// </summary>
        public List<Labyrinth> listOfAllCreatedRooms = new List<Labyrinth>();
        private int currentRoomRow;
        private const int MAX_ROOMS = 4;        //max depth of rooms
        private int currentRoomCount;
        private int currentRoomColumn;
        private Random random = new Random();
        public int num_enemies_max_per_room = 4;    //will be 1 less than specified
        public int endPointWorldNum;       //the world position (Labyrinth) where stairs down are located 
        public Point endPointStairs;       //Point where stairs are located

        public int CurrentRoomRow
        {
            get { return currentRoomRow; }
            set { currentRoomRow = value; }
        }

        public int CurrentRoomColumn
        {
            get { return currentRoomColumn; }
            set { currentRoomColumn = value; }
        }

        /// <summary>
        /// Moves to different room via door, and updates currentRoom. Removes player from old room, and puts him in new room
        /// </summary>
        /// <param name="door"></param>
        public void moveTo(Door door, Player player)
        {
            getCurrentRoom().creatures.Remove(player);
            currentRoomRow = door.RoomConnectionRow;
            currentRoomColumn = door.RoomConnectionCol;

            Door nextRoomDoor = returnNextRoomDoor(door.Position);
            player.moveObject(nextRoomDoor.xPos, nextRoomDoor.yPos);
            getCurrentRoom().addCreature(player);

        }

        /// <summary>
        /// Used by moveTo method in World-class to return the location of the next door. currentRoomRow/Col needs to be set first
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private Door returnNextRoomDoor(int position)
        {
            List<Entity> doorList = getCurrentRoom().architectureList;

            if (position == 0)     //door entered was in north of room. player appears in south of next room
            {
                foreach(Entity ent in doorList)
                {
                    Door door = ent as Door;
                    if (door.Position == 2)
                    {
                        return door;
                    }
                }
            }
            else if (position == 1)    //west. player appears in east of next room
            {
                foreach (Entity ent in doorList)
                {
                    Door door = ent as Door;
                    if (door.Position == 3)
                    {
                        return door;
                    }
                }
            }
            else if (position == 2)    //south. player appears in north of next room
            {
                foreach (Entity ent in doorList)
                {
                    Door door = ent as Door;
                    if (door.Position == 0)
                    {
                        return door;
                    }
                }
            }
            else if (position == 3)    //east. player appears in west
            {
                foreach (Entity ent in doorList)
                {
                    Door door = ent as Door;
                    if (door.Position == 1)
                    {
                        return door;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// First world and rooms are created, then doors, then walls around the rooms
        /// </summary>
        public World()
        {
            generateWorld();
            generateDoors();
            generateWalls();
            generateStairs();
            generateEnemies();
            generateLoot();
            
        }

        /// <summary>
        /// adds stairs up @ lab 0, point 1,1. Adds stairs down @ random lab (not in first room) in free point
        /// Notice the inverted Point class, this is confusing: TODO
        /// </summary>
        public void generateStairs()
        {
            listOfAllCreatedRooms[0].addDungeonArchitecture(new StairsUp(1,1)); //stairs up

            Labyrinth endLab = listOfAllCreatedRooms[listOfAllCreatedRooms.Count - 1]; //lab with stairs down

            if (endLab.freePositions.Count == 0)
            {
                for(int i = listOfAllCreatedRooms.Count-2; i >= 0; i--){
                    Labyrinth lab = listOfAllCreatedRooms[i];
                    if (lab.freePositions.Count > 0)
                    {
                        Point where = lab.freePositions[random.Next(lab.freePositions.Count)];
                        lab.addDungeonArchitecture(new StairsDown(where.Y, where.X));
                        endPointStairs = new Point(where.Y, where.X);
                        endPointWorldNum = i;
                        return;
                    }
                }
                Game.Debug_Mess("Could not create stairs down!");
            }
            else
            {
                Point where = endLab.freePositions[random.Next(endLab.freePositions.Count)];
                endLab.addDungeonArchitecture(new StairsDown(where.Y, where.X));
                endPointStairs = new Point(where.Y, where.X);
                endPointWorldNum = listOfAllCreatedRooms.Count-1;
            }


        }

        /// <summary>
        /// Generates loot for each room.
        /// Notice the inverted Point class, this is confusing: TODO
        /// </summary>
        public void generateLoot()
        {
            foreach (Labyrinth lab in listOfAllCreatedRooms)
            {
                if (random.Next(Game.LOOT_CHANCE) != 0)        //20% chance of loot
                    continue;

                int numLoot = helper.getWeightedSum(Game.LOOT_ITEMS_PER_ROOM, Game.LOOT_CHANCE);    //chance of more loot

                for (int i = 1; i <= numLoot; i++)
                {
                    Point p = lab.freePositionsLoot[random.Next(0, lab.freePositionsLoot.GetLength(0))];
                    Item itemToAdd = helper.getRandomLoot(lab, p.Y, p.X);
                    if (itemToAdd != null)
                        lab.groundObjects.Add(itemToAdd);
                }

            }

        }


        /// <summary>
        /// Randomly positions enemies in all the rooms, depends on field num_enemies_max_per_room.
        /// the list freePositions keeps track of all empty spaces on the level
        /// </summary>
        public void generateEnemies()
        {
            foreach(Labyrinth lab in listOfAllCreatedRooms){
                int numEnemies = random.Next(num_enemies_max_per_room);
                if(lab.freePositions.Count >0)
                    for (int curr = 0; curr < numEnemies; curr++)
                    {
                        Point p = lab.freePositions[random.Next(lab.freePositions.Count)];
                        addEnemy(p, lab);
                        lab.freePositions.Remove(p);    //removes this free position
                    }
            }
        }

        /// <summary>
        /// By default this positions the player @ lab with index 0. It also removes the point from lab.freePositions
        /// </summary>
        public void positionPlayer(Player player)
        {
            Labyrinth lab = listOfAllCreatedRooms[0];
            lab.creatures.Add(player);
            Point playerP = new Point(player.xPos,player.yPos);

            //this might crash if no freepositions
            for (int i = lab.freePositions.Count - 1; i >= 0; i--)
            {
                if (lab.freePositions[i].Equals(playerP))
                {
                    lab.freePositions.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Adds a singular enemy @ point p in specific labyrinth
        /// </summary>
        public void addEnemy(Point p, Labyrinth lab)
        {
            Creature creature = new Kobold(p.X, p.Y, helper.getAscii(Helper.KOBOLD_ASCII), "kobold", 2, 2, 3);  //just kobolds as placeholder
            creature.ai_attack_preference = Entity.ATTACK_UNARMED;
            lab.creatures.Add(creature);        
        }

        /// <summary>
        /// Generating walls on room borders and where there are no doors
        /// When generating extra walls (internal walls as decoration, u need to make sure they dont block doors and player positioning, make them +-2 from border)
        /// </summary>
        public void generateWalls()
        {

            for (int i = 0; i < listOfAllCreatedRooms.Count; i++)
            {
                Labyrinth lab = listOfAllCreatedRooms[i];
                char[,] currLab = lab.getLab();

                if (Game.DEBUG)
                {
                    lab.addDungeonArchitecture(new Wall(3, 5, helper.getAscii(Helper.WALL_ASCII)));
                    lab.addDungeonArchitecture(new Wall(4, 5, helper.getAscii(Helper.WALL_ASCII)));
                    lab.addDungeonArchitecture(new Wall(5, 5, helper.getAscii(Helper.WALL_ASCII)));
                    lab.addDungeonArchitecture(new Wall(6, 5, helper.getAscii(Helper.WALL_ASCII)));
                }

                for (int y = 0; y < currLab.GetLength(0); y++)
                {
                    for (int x = 0; x < currLab.GetLength(1); x++)
                    {
                        Entity aEnt = null;
                        lab.checkIfArchitecture(x, y, out aEnt);     //checking if other entity in the way (just need to check for doors really, for border placement)

                        if (x == 0 || y == 0 || x == currLab.GetLength(1) - 1 || y == currLab.GetLength(0) - 1)    //borders, should always be clear of monsters
                        {
                            if (aEnt == null)     //checking if other entity in the way for border (just need to check for doors really)
                            {
                                lab.addDungeonArchitecture(new Wall(x, y, helper.getAscii(Helper.WALL_ASCII)));
                            }


                        }
                        else                            //adding free space to the current lab
                        {
                            if (aEnt == null)
                                lab.freePositions.Add(new System.Drawing.Point(y, x));      //and this is inverted for whatever reason...
                        }

                    }
                }

                lab.freePositionsLoot = new Point[lab.freePositions.Count];     //copies free positions to array for loot placement etc.
                lab.freePositions.CopyTo(lab.freePositionsLoot,0);
            }
        }

        public Labyrinth getCurrentRoom()
        {
            return worldList[currentRoomRow, currentRoomColumn];
        }
        
        public void generateWorld() 
        {
            currentRoomColumn = worldList.GetLength(1) / 2;
            currentRoomRow = worldList.GetLength(0) / 2;
            Labyrinth lab = new Labyrinth(worldList.GetLength(0)/2,worldList.GetLength(1)/2);
            listOfAllCreatedRooms.Add(lab);
            Game.Debug_Mess("room created");
            worldList[worldList.GetLength(0) / 2, worldList.GetLength(1) / 2] = lab;
            addRoom(lab, MAX_ROOMS);
        }

        //printing all the rooms in the world as grid
        public void printWorld()
        {
            if (Game.DEBUG)
            {
                for (int row = 0; row < worldList.GetLength(0); row++)
                {
                    for (int column = 0; column < worldList.GetLength(1); column++)
                    {
                        if (worldList[row, column] == null)
                        {
                            Console.Write(".");
                        }
                        else
                        {
                            Console.Write("X");
                        }
                    }
                    Console.Write("\n");
                }
            }
        }

        public List<int> generateRandomRoomsWhere(Labyrinth startingRoom)
        {
            List<int> randRoomsList = new List<int>();

            if ((worldList[startingRoom.rowPosition + 1, startingRoom.colPosition] == null))    //south
            {
                randRoomsList.Add(2);
            }

            if ((worldList[startingRoom.rowPosition - 1, startingRoom.colPosition] == null))     //north
            {
                randRoomsList.Add(0);
            }

            if ((worldList[startingRoom.rowPosition, startingRoom.colPosition + 1] == null))     //east
            {
                randRoomsList.Add(3);
            }

            if ((worldList[startingRoom.rowPosition, startingRoom.colPosition - 1] == null))     //west
            {
                randRoomsList.Add(1);
            }

            return randRoomsList;
        }

        public void addRoom(Labyrinth startingRoom, int currentDepth)
        {

            //int numRoomsToCreate = random.Next(1, 5);                       //number of doors to try to create
            int numRoomsToCreate = helper.getWeightedSum(4,5);
            List<int> pullFromThis = generateRandomRoomsWhere(startingRoom);    //possible door locations
            for(int i = 0; i < numRoomsToCreate; i++)
            {
                if (currentDepth > 0)
                {

                    if (pullFromThis.Count > 0)
                    {
                        int randIndex = random.Next(0,pullFromThis.Count);
                        int number = pullFromThis[randIndex];
                        pullFromThis.RemoveAt(randIndex);

                        if (number == 0)
                        {
                            Game.Debug_Mess("room created");
                            Labyrinth lab = new Labyrinth(startingRoom.rowPosition - 1, startingRoom.colPosition);
                            listOfAllCreatedRooms.Add(lab);
                            worldList[startingRoom.rowPosition - 1, startingRoom.colPosition] = lab;
                            currentRoomCount++;
                            addRoom(lab,currentDepth-1);
                        }
                        else if (number == 1){
                            Game.Debug_Mess("room created");
                            Labyrinth lab = new Labyrinth(startingRoom.rowPosition, startingRoom.colPosition - 1);
                            listOfAllCreatedRooms.Add(lab);
                            worldList[startingRoom.rowPosition, startingRoom.colPosition - 1] = lab;
                            currentRoomCount++;
                            addRoom(lab, currentDepth - 1);
                        }
                        else if (number == 2)
                        {
                            Game.Debug_Mess("room created");
                            Labyrinth lab = new Labyrinth(startingRoom.rowPosition + 1, startingRoom.colPosition);
                            listOfAllCreatedRooms.Add(lab);
                            worldList[startingRoom.rowPosition + 1, startingRoom.colPosition] = lab;
                            currentRoomCount++;
                            addRoom(lab, currentDepth - 1);
                        }
                        else if (number == 3)
                        {
                            Game.Debug_Mess("room created");
                            Labyrinth lab = new Labyrinth(startingRoom.rowPosition, startingRoom.colPosition + 1);
                            listOfAllCreatedRooms.Add(lab);
                            worldList[startingRoom.rowPosition, startingRoom.colPosition + 1] = lab;
                            currentRoomCount++;
                            addRoom(lab, currentDepth - 1);
                        }
                    }
                   
                }

            }
        }

        /// <summary>
        /// Loops through all the rooms, and checks if there are adjacent rooms. Adds a door if not, linked to that (next) room
        /// </summary>
        public void generateDoors()
        {
            for (int i = 1; i < worldList.GetLength(0)-1; i++)
            {
                for (int y = 1; y < worldList.GetLength(1)-1; y++)
                {
        
                    Labyrinth currentLab = worldList[i, y];

                    if (currentLab != null)
                    {
                        char[,] labArray = currentLab.getLab();
                        int doorPosListNumber = 0;

                        if (worldList[i, y - 1] != null)       //west
                        {
                            currentLab.addDungeonArchitecture(new Door(0, labArray.GetLength(0) / 2, new int[] { i, y - 1 }, 1, helper.getAscii(Helper.DOOR_ASCII)));
                            currentLab.doorPositionsForMap[doorPosListNumber] = Labyrinth.DOOR_POS_W;
                            doorPosListNumber++;

                        }

                        if (worldList[i, y + 1] != null)       //east
                        {
                            currentLab.addDungeonArchitecture(new Door(labArray.GetLength(1) - 1, labArray.GetLength(0) / 2, new int[] { i, y + 1 }, 3, helper.getAscii(Helper.DOOR_ASCII)));
                            currentLab.doorPositionsForMap[doorPosListNumber] = Labyrinth.DOOR_POS_E;
                            doorPosListNumber++;
                        }

                        if (worldList[i - 1, y] != null)       //north
                        {
                            currentLab.addDungeonArchitecture(new Door(labArray.GetLength(1) / 2, 0, new int[] { i - 1, y },0, helper.getAscii(Helper.DOOR_ASCII)));
                            currentLab.doorPositionsForMap[doorPosListNumber] = Labyrinth.DOOR_POS_N;
                            doorPosListNumber++;
                        }

                        if (worldList[i + 1, y] != null)       //south
                        {
                            currentLab.addDungeonArchitecture(new Door(labArray.GetLength(1) / 2, labArray.GetLength(0) - 1, new int[] { i + 1, y }, 2, helper.getAscii(Helper.DOOR_ASCII)));
                            currentLab.doorPositionsForMap[doorPosListNumber] = Labyrinth.DOOR_POS_S;
                        }
                    }
                }
            }
        }

    }
}
