﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DungeonForms
{
    /// <summary>
    /// Very simple "animation" class. Just showing a static image for a time, then removing it. Not used in ascii only mode
    /// </summary>
    public class Animation : Gfx
    {
        public const long DURATION_SHORT = 250;
        Timer timer;
        public bool Finished;

        public Animation(int xPos, int yPos, int sizeX, int sizeY, Bitmap bmp, long duration)
            : base(xPos,yPos,sizeX,sizeY,bmp)
        {
            timer = new Timer(duration);
            timer.AutoReset = false;
            timer.Elapsed += timerTrigger;
            timer.Start();
        }

        private void timerTrigger(object sender, ElapsedEventArgs e)
        {
            timer.Dispose();
            Finished = true;
        }


    }
}
