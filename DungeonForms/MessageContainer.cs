﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonForms
{

   
    /// <summary>
    /// Saves down messages from combat etc, for displaying in forms or in console.
    /// You need to clear it at the beginning of each new turn.
    /// </summary>
    public class MessageContainer
    {
        /// <summary>
        /// action messages
        /// </summary>
        private String message;
        
        /// <summary>
        /// the message returned when the user mouses over, used for forms application
        /// </summary>
        private String lookAtThisMessage;

        public String LootAtThisMess
        {
            get { return lookAtThisMessage; }
        }

        public String Message
        {
            get { return message; }
        }

        private static MessageContainer container = new MessageContainer();

        public static MessageContainer getContainer()
        {
            return container;
        }

        public void addLookAtMess(String mess)
        {
            lookAtThisMessage += mess + "\n";
        }

        public void clearLookAt()
        {
            lookAtThisMessage = "";
        }

        public void addMessage(String mess)
        {
            message += (mess+"\n");
            if (Game.DEBUG)
                Console.WriteLine(this.GetType().FullName + ": " + message);
        }

        public void clearMessages()
        {
            message = "";
        }



    }
}
