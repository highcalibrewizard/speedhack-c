﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DungeonForms
{
    /// <summary>
    /// Simple class for holding a bitmap.
    /// </summary>
    public class Gfx
    {
        public int xPos;
        public int yPos;
        public int sizeX;
        public int sizeY;
        public Bitmap bmp;

        public Gfx(int posX, int posY, int sizeX, int sizeY, Bitmap bmp) {
            this.xPos = posX;
            this.yPos = posY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.bmp = bmp;
        }
    }
}
