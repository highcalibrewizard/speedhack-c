﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonForms.entities;

namespace DungeonForms
{
    public class Helper
    {
        Random random;
        private Dictionary<String, char> asciiIconDict = new Dictionary<string, char>();       
        private static Helper helper = new Helper();
        public const string WALL_ASCII = "wall";
        public const string PLAYER_ASCII = "player";
        public const string FLOOR_ASCII = "floor";
        public const string DOOR_ASCII = "door";
        public const string KOBOLD_ASCII = "kobold";
        public const string LOOTBAG_ASCII = "lootbag";
        public const string STAIRSUP_ASCII = "stairsup";
        public const string STAIRSDOWN_ASCII = "stairsdown";

        List<Item> lootTable = new List<Item>();
        private int lootSum;


        private Helper()
        {
            // {"wall":'=',"player":'@',"floor":'.',"door":'X'}
            asciiIconDict.Add(WALL_ASCII, '#');
            asciiIconDict.Add(PLAYER_ASCII, '@');
            asciiIconDict.Add(FLOOR_ASCII, '.');
            asciiIconDict.Add(DOOR_ASCII, '/');
            asciiIconDict.Add(KOBOLD_ASCII, 'k');
            asciiIconDict.Add(LOOTBAG_ASCII, 'b');
            asciiIconDict.Add(STAIRSDOWN_ASCII, '>');
            asciiIconDict.Add(STAIRSUP_ASCII, '<');

            random = new Random();
            generateLootTable();


        }


        /// <summary>
        /// Adds all loot with their weight, this is a manual process as of now.
        /// Todo: probably need to add level requirement or something similar
        /// </summary>
        private void generateLootTable()
        {
            OneHanded sword = new OneHanded(0, 0, "Dummy onehand",Game.LOOT_RARITY_COMMON,lootSum);
            lootSum += sword.CeilW;
            OffHand shield = new OffHand(0, 0, "Dummy offhand", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;
            TwoHanded twoHand = new TwoHanded(0, 0, "Dummy twohand", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;
            ArmorBody bodyArmor = new ArmorBody(0, 0, "Dummy armor", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;
            Potion potion = new Potion(0, 0, "Dummy usable", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;
            RangedTwoHand rifle = new RangedTwoHand(0, 0, "Dummy ranged twohand", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;
            RangedOneHand pistol = new RangedOneHand(0, 0, "Dummy ranged onehand", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;
            ThrowableOneHand grenade = new ThrowableOneHand(0, 0, "Dummy throwable", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;
            ThrowableTwoHand throwingSpear = new ThrowableTwoHand(0, 0, "Dummy twohand throwable", Game.LOOT_RARITY_COMMON, lootSum);
            lootSum += sword.CeilW;     //needs to have -1 last, but uses random.Next so it's not necessary here

            lootTable.Add(sword);
            lootTable.Add(shield);
            lootTable.Add(twoHand);
            lootTable.Add(bodyArmor);
            lootTable.Add(potion);
            lootTable.Add(rifle);
            lootTable.Add(pistol);
            lootTable.Add(grenade);
            lootTable.Add(throwingSpear);

            Game.Debug_Mess("Loot sum is " + lootSum);
        }

        /// <summary>
        /// returns a random item as loot, or null if no loot
        /// </summary>
        /// <returns></returns>
        public Item getRandomLoot(Labyrinth lab, int xPos, int yPos)
        {

            int rand = random.Next(lootSum);

            foreach (Item item in lootTable)
            {
                if (rand >= item.FloorW && rand <= item.CeilCompareToThis)
                {
                    Game.Debug_Mess("generated " + item.name);
                    Item newItem = checkItemBaseForCloning(item,xPos,yPos);
                    return newItem;
                }
                
            }

            Game.Debug_Mess("in getrandomloot, could not find loot for some reason");
            return null;
        }

        private Item checkItemBaseForCloning(Item item,int xPos, int yPos)
        {

            if (item is ArmorBody)
            {
                return new ArmorBody(item as ArmorBody, xPos, yPos);
            }
            else if (item is Potion)
            {
                return new Potion(item as Potion, xPos, yPos);
            }
            else if (item is RangedTwoHand)
            {
                return new RangedTwoHand(item as RangedTwoHand, xPos, yPos);
            }
            else if (item is RangedOneHand)
            {
                return new RangedOneHand(item as RangedOneHand, xPos, yPos);
            }
            else if (item is ThrowableOneHand)
            {
                return new ThrowableOneHand(item as ThrowableOneHand, xPos, yPos);
            }
            else if (item is ThrowableTwoHand)
            {
                return new ThrowableTwoHand(item as ThrowableTwoHand, xPos, yPos);
            }
            else if (item is OffHand)
            {
                return new OffHand(item as OffHand, xPos, yPos);
            }
            else if (item is OneHanded)     //NEEDS TO BE LAST!
            {
                return new OneHanded(item as OneHanded,xPos,yPos);
            }
            else if (item is TwoHanded)     //NEEDS TO BE LAST!
            {
                return new TwoHanded(item as TwoHanded, xPos, yPos);
            }
            else
            {
                Game.Debug_Mess("could not find base type in checkItemBaseForCloning"); //if you get this message, check so the item base is implemented in the if-else structure
                return null;
            }
        }

        /// <summary>
        /// Returns a weighted sum where 1 has 100% to show up, and then for each step the probability is decreased by increased (2 is 50% chance, 3 is 33%, 4 is 25% etc)
        /// </summary>
        /// <param name="max"></param>
        /// <param name="increase"></param>
        public int getWeightedSum(int max, int increase){
            int sum = 1;

            for (int i = 2; i <= max; i++)
            {
                int curr = random.Next(i,i+increase);
                if (curr == i)
                {
                    sum++;
                }
            }

            return sum;
        }

        public void debugMess(String mess){
            if (Game.DEBUG)
                Console.WriteLine(mess);
        }

        public char getAscii(String type)
        {
            char val;
            if (asciiIconDict.TryGetValue(type, out val))
            {
                return val;
            }
            Game.Debug_Mess(String.Format("Ascii icon {0} not found", type));
            return '.';
            
        }

        public static Helper getInstance()
        {
            return helper;
        }
    }
}
